## Problem to solve [#Card]()

<!-- What is the user problem you are trying to solve with this issue? -->
<!-- 해결하고자하는 유저의 문제 또는 기능 개발/개선 이유 작성 -->

## Proposal

<!-- Use this section to explain the feature and how it will work. It can be helpful to add technical details, design proposals, and links to related epics or issues. -->
<!-- 제안하고자하는 기능과 동작 방식을 설명. 기술적인 세부사항, 디자인 제안, 참고할만한 자료 링크(이슈, 문서, 페이지 등)를 추가하면 도움이 됨  -->

## Availability & Testing

<!-- This section needs to be retained and filled in during the workflow planning breakdown phase of this feature proposal, if not earlier.

What risks does this change pose to our availability? How might it affect the quality of the product? What additional test coverage or changes to tests will be needed? Will it require cross-browser testing?

Please list the test areas (unit, integration and end-to-end) that needs to be added or updated to ensure that this feature will work as intended. Please use the list below as guidance.
* Unit test changes
* Integration test changes
* End-to-end test change

See the test engineering planning process and reach out to your counterpart Software Engineer in Test for assistance: https://about.gitlab.com/handbook/engineering/quality/test-engineering/#test-planning  -->
<!-- workflow를 계획할 때 아래와 같은 고민을 하면서 이 섹션을 작성해 나갈 것.
 - 이 변경으로 인해 발생할 수 있는 위험요소는?
 - 서비스의 품질에 어떤 영향을 미칠 수 있나?
 - 추가 테스트 적용 범위 또는 테스트 변경이 필요한가?
 - 브라우저 간 테스트가 필요한가? 등

이 기능이 의도 한대로 작동하는지 확인하기 위해 추가하거나 업데이트해야하는 테스트 영역 (유닛, 통합 및 종단 간)을 나열하십시오.
 - 유닛 테스트 변경
 - 통합 테스트 변경
 - 종단 간 테스트 변경
-->

## What does success look like, and how can we measure that?

<!-- Define both the success metrics and acceptance criteria. Note that success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this. -->
<!-- 성공 지표와 인수 기준을 모두 정의하기. 성공 지표는 원하는 비즈니스 결과(예, 페이지 로드 속도가 개선되어 관련 CS 발생률이 20%줄어듬)를 나타내고 인수 기준은 "실제 환경에서 사용가능하다"는 판단을 내릴 수 있는 기준을 나타냄. -->

## Implementation plan
<!-- Steps and the parts of the code that will need to get updated. The plan can also call-out responsibilities for other team members or teams.
  - [ ] ~frontend Step 1
    - [ ] `@person` Step 1a
  - [ ] ~frontend Step 2
-->
<!-- 구현 계획 및 순서 -->

## Other links/references

<!-- E.g. related GitLab issues/MRs, page, docs, etc -->
<!-- 예) 관련 GitLab issues/MRs, 자료, docs 등 -->

/label ~feature
