## Summary [#Card]()

<!-- Please briefly describe what part of the code base needs to be refactored. -->
<!-- 코드 베이스의 어느 부분을 리팩터해야하는지 간략하게 설명 -->

## Improvements

<!-- Explain the benefits of refactoring this code.
See also https://about.gitlab.com/handbook/values/index.html#say-why-not-just-what -->
<!-- 이 코드를 리팩터링하면 얻는 이득 작성 -->

## Risks

<!-- Please list features that can break because of this refactoring and how you intend to solve that. -->
<!-- 이 리팩터링을 진행하면 영향을 받을 수 있는 기능 및 해결 방법 작성 -->

## Involved components

<!-- List files or directories that will be changed by the refactoring. -->
<!-- 이 리팩터링으로 변경된 파일 또는 폴더 작성 -->

## (Optional) Intended side effects

<!-- If the refactoring involves changes apart from the main improvements (such as a better UI), list them here. It may be a good idea to create separate issues and link them here. -->
<!-- 이 리팩터링에 주요 개선사항 외의 변경 사항이 있는 경우 기술. 별도 issue로 새로 만들어서 여기에 링크로 연결하는 것을 권장 -->

## (Optional) Missing test coverage

<!-- If you are aware of tests that need to be written or adjusted apart from unit tests for the changed components, please list them here. -->
<!-- 변경된 구성 요소에 대한 단위 테스트 또는 추가로 작성/조정해야하는 테스트가 있다면 작성 -->

## Other links/references

<!-- E.g. related GitLab issues/MRs, page, docs, etc -->
<!-- 예) 관련 GitLab issues/MRs, 자료, docs 등 -->

/label ~maintenance