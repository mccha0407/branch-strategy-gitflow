## Summary [#Card]()

<!-- Summarize the bug encountered concisely. Add notion card link -->
<!-- 발생한 버그 간결하게 요약하기. Notion 카드링크 달기 -->

## Steps to reproduce

<!-- Describe how one can reproduce the issue - this is very important. Please use an ordered list. -->
<!-- (매우 중요!!) 이슈 재현하는 방법. ordered list를 사용하여 작성 -->

## What is the current *bug* behavior?

<!-- Describe what actually happens. -->
<!-- 실제로 발생한 것 작성  -->
### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->
<!-- 관련 로그 또는 코드를 코드 블럭(```)을 사용하여 작성하거나 스크린샷 첨부  -->

## What is the expected *correct* behavior?

<!-- Describe what you should see instead. -->
<!-- 정상일 때의 모습(동작) 등을 작성 -->

### Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code
 as it's tough to read otherwise. -->
<!-- 관련 로그 또는 코드를 코드 블럭(```)을 사용하여 작성하거나 스크린샷 첨부  -->

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem. -->
<!-- (예상이 되는) 문제의 원인으로 보이는 코드 라인 또는 이유. -->

## Implementation plan
<!-- Steps and the parts of the code that will need to get updated. The plan can also call-out responsibilities for other team members or teams.
  - [ ] ~frontend Step 1
    - [ ] `@person` Step 1a
  - [ ] ~frontend Step 2
-->
<!-- 구현 계획 및 순서 -->

## Other links/references

<!-- E.g. related GitLab issues/MRs, page, docs, etc -->
<!-- 예) 관련 GitLab issues/MRs, 자료, docs 등 -->

/label ~bug